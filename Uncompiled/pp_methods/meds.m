function [Xmedians, Xmedian] = meds(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmedian = nanmedian(x,2);
Xmedians = x./repmat(Xmedian,1,m);
