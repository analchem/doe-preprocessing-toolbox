function [Xnew, Xstd]=rnv(X,p)
%   Xnew=rnv(X,p);
%   Xnew is the robust normal variated transformed matrix X
[n,m]=size(X);
Xnew=zeros(n,m);
prct=prctile(X,p,2);

for o=1:n
    Xstd(o,1) = std(X(o,X(o,:)<=prct(o)));
    Xnew(o,:)=(X(o,:)-prct(o))/Xstd(o,1);  
end
