function ynew=detrpoly(y,order)
[n,m]=size(y);
if nargin==1
    order=2;
end
if m==1 && n==1
    error('vector or matrix are expected as input')
end
X=(1:m);
ynew=zeros(n,m);
for o=1:n
        ynew(o,:)=y(o,:)-polyval(polyfit(X,y(o,:),order),X);
end