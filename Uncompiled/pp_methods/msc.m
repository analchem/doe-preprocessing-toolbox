function [Xc,a,b]=msc(X,Xref)
%   [Xc,a,b]=msc(X,Xref);
[n,m]=size(X);
if nargin<2
    Xref=mean(X);
end
if Xref == 0
    Xref = mean(X);
end
for i=1:n,							
  p=polyfit(Xref,X(i,:),1);			
  Xc(i,:)=(X(i,:)-p(2)*ones(1,m))./(p(1)*ones(1,m));
  a(i,1) = p(2); b(i,1) = p(1);
end 



