function [Xmins Xmin] = mins(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmin = min(x,[],2);
Xmins = x./repmat(Xmin,1,m);
