function [Xmeans, Xmean] = means(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmean = mean(x,2);
if sum(Xmean==0)>0 %so if by accident a sample has 0 mean
    zeromean = find(Xmean==0);
    for Q = 1:length(zeromean)
        Xmean(zeromean(Q)) = Xmean(zeromean(Q)) + 0.05 * mean(Xmean); %slightly adjust the zero mean based on all other means
    end
end
Xmeans = x./repmat(Xmean,1,m);
