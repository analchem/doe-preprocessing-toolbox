function [Xmeans, Xmean] = means(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmean = mean(x,2);
Xmeans = x./repmat(Xmean,1,m);
