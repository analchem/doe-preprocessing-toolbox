function [Xbl, bl] = AsLs(X, lambda, p)
% Estimate baseline with asymmetric least squares
% [z, bl] = AsLs(X, lambda, p)
% lambda = smoothing factor (default 1e7; range 1e2 -- 1e9)
% p      = asymetry parameter (default 0.01; range 1e-3 -- 1e-1)
%
% Reference: 
% P.H.C. Eilers (2004),"Parametric time warping", Analytical Chemistry, 
% 76, 404 - 411 
%
% P.H.C. Eilers and H.F.M. Boelens (2005), "Baseline correction with asymmetric
% least squares smoothing", draft publication

switch nargin 
    case 1
    lambda = 1e7;
    p=.01;
    case 2      
    p=.01;
end

[n,~]=size(X);

for i=1:n
    y=X(i,:);
    y=y';
    m = length(y);
    D = diff(speye(m), 2);
    w = ones(m, 1);
    for it = 1:10
        W = spdiags(w, 0, m, m);
        C = chol(W + lambda * D' * D);
        z = C \ (C' \ (w .* y));
        w = p * (y > z) + (1 - p) * (y < z);
    end
    z = z';
    bl (i,:)=z;
    Xbl(i,:)=X(i,:)-z;
end


