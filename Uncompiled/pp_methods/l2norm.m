function [xNorms, Normx] = l2norm(x)

[n,~]=size(x);
for i=1:n
    Normx(i,:) = norm(x(i,:));
    xNorms(i,:) = x(i,:)/Normx(i,:);  
end
