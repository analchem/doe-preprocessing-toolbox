function [Xsnv, Xmean, Xstd] = snv(x)
%   SNV transforms data matrix x row wise
%   
%   Input:
%   x = data matrix
%   
%   Output:
%   xSNV = SNV transformed data matrix

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmean=mean(x,2);
Xstd=std(x,0,2);
Xsnv = (x-repmat(Xmean,1,m))./repmat(Xstd,1,m);
