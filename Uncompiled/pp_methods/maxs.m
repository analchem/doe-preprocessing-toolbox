function [Xmaxs, Xmax] = maxs(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmax = max(x,[],2);
Xmaxs = x./repmat(Xmax,1,m);
