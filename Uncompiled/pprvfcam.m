function [RMSECV_optmodel, vars_removed, complexity_optmodel] = pprvfcam(HuhnSc, Datashuf, Labelshuf, Scaling, ...
    MaxLV, testmatrix, l, m, n, RMSECV_fs)
%UNTITLED PPRV-FCAM, as described by Andries et al.

%HuhnSc is given as input, to determine the regression coefficient values;
%Datashuf and Labelshuf are required to obtain RMSECV. Pre-processing is
%applied only once, and not repeated after each variable has been removed.

%FOR USE WITHIN THE EXPERIMENTAL DESIGN APPROACH ONLY, NOT MEANT TO BE RUN
%SEPARATELY

global foutvector
global counter

for iter = 1:size(testmatrix,1) %iter: amount of train-test-divisions
    DataTest = Datashuf(testmatrix(iter,1):testmatrix(iter,2), :);
    LabelTest{iter} = Labelshuf(testmatrix(iter,1):testmatrix(iter,2));
    DataTrain = Datashuf;
    DataTrain(testmatrix(iter,1):testmatrix(iter,2), :) = [];
    LabelTrain{iter} = Labelshuf;
    LabelTrain{iter}(testmatrix(iter,1):testmatrix(iter,2)) = [];

    %scale train data and test data with parameters from train data
    [DataTrainSc{iter}, DataTestSc{iter}] = pretreat_scale(Scaling, DataTrain, ...
        DataTest, [], 0.03);

    %Check for NaNs of Infs in scaled data
    if (sum(isnan(DataTrainSc{iter}(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        RMSECV_optmodel=40;
        complexity_optmodel=40;
        vars_removed = NaN;
        return
    end
    if (sum(isinf(DataTrainSc{iter}(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        RMSECV_optmodel=40;
        complexity_optmodel=40;
        vars_removed = NaN;
        return
    end
    if (sum(isnan(DataTestSc{iter}(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        RMSECV_optmodel=40;
        complexity_optmodel=40;
        vars_removed = NaN;
        return
    end
    if (sum(isinf(DataTestSc{iter}(:)))~= 0) 
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        RMSECV_optmodel=40;
        complexity_optmodel=40;
        vars_removed = NaN;
        return
    end
end

bla = (size(HuhnSc, 2) - 1);

vecnum = 1:size(HuhnSc, 2);

for iter = 1:(size(HuhnSc, 2) - MaxLV)
    B = pls_Jan(HuhnSc, Labelshuf, MaxLV);
    B = abs(B(2:end)); %B(1) is offset, not required here
    [~, removevar] = min(B); %variable with lowest absolute regression coefficient
    
    variables_removed(iter) = vecnum(removevar);
    vecnum(removevar) = [];

    %Remove var with lowest abs(B) from data
    HuhnSc(:, removevar) = [];
    for i = 1:size(testmatrix,1)
        DataTrainSc{i}(:,removevar) = [];
        DataTestSc{i}(:,removevar) = [];
    end
    
    %Obtain new RMSECV
    for i = 1:size(testmatrix,1)
        B = pls_Jan(DataTrainSc{i}, LabelTrain{i}, MaxLV);
        Concpred = (DataTestSc{i} * B(2:end)) + B(1);
        classvecmat(1,testmatrix(i,1):testmatrix(i,2)) = LabelTest{i};
        classvecmat(2,testmatrix(i,1):testmatrix(i,2)) = Concpred;
    end
    RMSEvec(iter) = rmse_Jan(classvecmat(1,:), classvecmat(2,:));
    complexityvec(iter) = MaxLV;
end

for iter = (size(Datashuf, 2) - MaxLV + 1):(size(Datashuf, 2)-1) %not size(HuhnSc) because that has already been reduced
    B = pls_Jan(HuhnSc, Labelshuf, MaxLV);
    B = abs(B(2:end));
    [~, removevar] = min(B); %variable with lowest absolute regression coefficient
    
    variables_removed(iter) = vecnum(removevar);
    vecnum(removevar) = [];
    
    MaxLV = MaxLV - 1;

    %Remove var with lowest abs(B) from data
    HuhnSc(:, removevar) = [];
    for i = 1:size(testmatrix,1)
        DataTrainSc{i}(:,removevar) = [];
        DataTestSc{i}(:,removevar) = [];
    end
    
    %Obtain new RMSECV
    for i = 1:size(testmatrix,1)
        B = pls_Jan(DataTrainSc{i}, LabelTrain{i}, MaxLV);
        Concpred = (DataTestSc{i} * B(2:end)) + B(1);
        classvecmat(1,testmatrix(i,1):testmatrix(i,2)) = LabelTest{i};
        classvecmat(2,testmatrix(i,1):testmatrix(i,2)) = Concpred;
    end
    RMSEvec(iter) = rmse_Jan(classvecmat(1,:), classvecmat(2,:));
    complexityvec(iter) = MaxLV;
end

% Obtain best model
[RMSECV_min, nvar_removed] = min(RMSEvec);
RMSECV_crit = sqrt(finv(0.95, size(Datashuf, 1), size(Datashuf, 1)) * RMSECV_min^2);

if RMSECV_crit > RMSECV_fs
    RMSECV_crit = RMSECV_fs;
end

%check for each variable set with more removed vars than nvar_removed,
%whether the RMSECV of that set is smaller than or equal to RMSECV_crit

for i = nvar_removed:length(RMSEvec)
    if RMSEvec(i) <= RMSECV_crit
        blab(i) = 1;
    else
        blab(i) = 0;
    end
end

opt_model = max(find(blab>0)); %find set with most variables removed, that still has a RMSECV <= RMSECV_crit

if isempty(opt_model)
    RMSECV_optmodel = RMSECV_fs;
    vars_removed = 'none';
    complexity_optmodel = MaxLV;
else
    RMSECV_optmodel = RMSEvec(opt_model);
    vars_removed = variables_removed(1:opt_model);
    complexity_optmodel = complexityvec(opt_model);
end
    
end

