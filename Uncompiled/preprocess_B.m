function [Xpre] = preprocess_B(x, id, par)

%   id =   1 Baseline offset removel (subtract minimum for each spectrum)
%          2 Linear baseline correction (subtract linear fit through first
%          and last point for each spectrum)
%          3 Baseline offset and linear baseline correction (perform second
%          method followed by first method).
%	       4 Detrending with polynomial function (2nd order)
%          5 Detrending with polynomial function (3rd order)
%          6 Detrending with polynomial function (4th order)
%          7 Asymmetric least squares smoothing (settings based on spectr)
%          8 Savitzsky-golay derivatisation (1st)
%          9 Savitzsky-golay derivatisation (2nd)

switch id
    case 1
        Xpre = x - (min(x')') * ones(1, size(x, 2));
    case 2
        Xpre = x - (((x(:, size(x, 2)) - x(:, 1)) ./ size(x, 2)) .* (ones(size(x, 1), 1) * (1:size(x, 2)))) - (x(:, 1) * ones(1, size(x, 2)));
    case 3
        x = x - (((x(:, size(x, 2)) - x(:, 1)) ./ size(x, 2)) .* (ones(size(x, 1), 1) * (1:size(x, 2)))) - (x(:, 1) * ones(1, size(x, 2)));
        Xpre = x - (min(x')') * ones(1, size(x, 2));
    case 4 
        Xpre = detrpoly(x,par(5));
    case 5 
        Xpre = detrpoly(x,par(6));
    case 6 
        Xpre = detrpoly(x,par(7));
    case 7 
        [Xpre, ~] = AsLs(x, par(8), par(9));
    case 8 
        [Xpre, ~] = savgol(x,par(12),2,1);
    case 9
        [Xpre, ~] = savgol(x,par(12),2,2);
end



