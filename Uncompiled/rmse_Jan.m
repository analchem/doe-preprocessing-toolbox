function RMSE=rmse_Jan(Y, YP)

[m n]=size(Y);

for i=1:n
    for j=1:m
        e(j,i)=(Y(j,i)-YP(j,i))^2;
    end
end

som=sum(sum(e));
RMSE=sqrt((1/(m*n))*som);