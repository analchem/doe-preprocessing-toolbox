function [Xpre] = preprocess_St(x, id, par)

%   id =    1 mean scaling
%           2 median scaling
%           3 max scaling
%           4 L2 norm scaling
%           5 SNV transformation
%           6 RNV transformation, setting 1 (15%)
%           7 RNV transformation, setting 2 (25%)
%           8 RNV transformation, setting 3 (35%)
%           9 MSC

switch id
    case 1
        [Xpre] = means(x);
    case 2
        [Xpre] = meds(x);
    case 3
        [Xpre] = maxs(x);
    case 4
        [Xpre] = l2norm(x);
    case 5
        [Xpre] = snv(x);
    case 6
        [Xpre]=rnv(x,par(1));
    case 7
        [Xpre]=rnv(x,par(2));
    case 8
        [Xpre]=rnv(x,par(3));
    case 9
        [Xpre]=msc(x,par(4));
end