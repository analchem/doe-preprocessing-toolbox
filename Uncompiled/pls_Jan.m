function [B, SCX, SCY, LX, LY]=pls_Jan(X, Y, LV)

%Script adapted on June 12, 2013
%Now, the script can correctly perform PLS2 regression as well as PLS1

MCX=X;
MCY=Y;

for i=1:LV
    
    S=MCX'*MCY;
    %[SCR, LDS, VAR]=pca(S);
    %W(:,i)=SCR(:,1); %28-03-2011: Dit zijn niet de first left singular vectors zoals in het dictaat wordt gevraagd! Beter is svd op S en dan U(:,1) nemen.
    [U, D, V]= svd(S, 'econ');
    W(:,i)=U(:,1);
    T(:,i)=MCX*W(:,i);
    P(:,i)=(MCX'*T(:,i))/(T(:,i)'*T(:,i));
    Q(:,i)=(MCY'*T(:,i))/(T(:,i)'*T(:,i));

    MCX=MCX-(T(:,i)*P(:,i)');
    MCY=MCY-(T(:,i)*Q(:,i)');
end

SCX=T;
SCY=Y*Q;
LX=P;
LY=Q;

R=W*inv(P'*W);
A=inv(T'*T)*T'*Y;
D=R*A;
[m n]=size(D);
B=zeros(m,n);

for i=1:size(Y,2)
    C=mean(Y(:,i))-(X*D(:,i));
    B(2:(m+1),i)=D(:,i);
    B(1,i)=mean(C);
end
