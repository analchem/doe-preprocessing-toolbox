function [Accur, NumLV, classvecmat] = plsldagcv_vdvoet_lv_selection(Datashuf, Labelshuf, Scaling, ...
    MaxLV, testmatrix, Opts, l, m, n, pvaluevoet)

%Function to perform LDA on PLS scores, including a cross-validation step
%which selects the optimal number of LVs, based on the lowest prediction
%error. Cross-validation is done based on 'leave-Perc%-out'.
%This function should be called from the higher-level function
%cubes_groot_plsda_goedecrossval.

%INPUT
%Data: the raw data
%Labels: labels of raw data
%Perc: percentage for leave-Perc%-out (Perc=0.1, 0.15, ...)
%Scaling: name of the scaling method to be included in the cross-validation
%MaxLV: maximum amount of LVs to consider
%Opts: structure with settings for pls to suppress command-line-output
%l, m, k, n: to store values in the 'foutvector'

%OUTPUT
%Accur: accuracy of the best result
%NumLV: number of LVs belonging to the highest accuracy

%Jan Gerretzen, 2013-01-09

global foutvector
global counter

%-----

%Loop
for iter = 1:size(testmatrix,1) %iter: amount of train-test-divisions
    DataTest = Datashuf(testmatrix(iter,1):testmatrix(iter,2), :);
    LabelTest = Labelshuf(testmatrix(iter,1):testmatrix(iter,2));
    DataTrain = Datashuf;
    DataTrain(testmatrix(iter,1):testmatrix(iter,2), :) = [];
    LabelTrain = Labelshuf;
    LabelTrain(testmatrix(iter,1):testmatrix(iter,2)) = [];
    
    %scale train data and test data with parameters from train data
    [DataTrainSc, DataTestSc] = pretreat_scale(Scaling, DataTrain, ...
        DataTest, [], 0.03);
    
    %Check for NaNs of Infs in scaled data
    if (sum(isnan(DataTrainSc(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        Accur=0;
        NumLV=40;
        classvecmat = NaN;
        return
    end
    if (sum(isinf(DataTrainSc(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        Accur=0;
        NumLV=40;
        classvecmat = NaN;
        return
    end
    if (sum(isnan(DataTestSc(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        Accur=0;
        NumLV=40;
        classvecmat = NaN;
        return
    end
    if (sum(isinf(DataTestSc(:)))~= 0) 
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        Accur=0;
        NumLV=40;
        classvecmat = NaN;
        return
    end
    
    for iterlv = 1:MaxLV
    B = pls_Jan(DataTrainSc, LabelTrain, iterlv);
    Concpred = (DataTestSc * B(2:end)) + B(1);    
    classvecmat{iterlv}(1,testmatrix(iter,1):testmatrix(iter,2)) = LabelTest;
    classvecmat{iterlv}(2,testmatrix(iter,1):testmatrix(iter,2)) = Concpred;
    %Perc_correct = sum((compmat(:,1)-compmat(:,2))==0);
    %accurmat(iterlv, iter) = Perc_correct;

    clear class
    clear compmat
    clear Perc_correct
    end
    
    clear DataTest
    clear LabelTest
    clear DataTrain
    clear LabelTrain
    clear DataTrainSc
    clear DataTestSc
    clear model
    clear DataTrainScore
    clear pred
    clear DataTestScore
end

%Selection of optimal no. of LVs, based on selection by Hilko van der Voet,
%Chemolab, 1994.

%Calculate p-values
for I = 1:(MaxLV - 1)
    eA = classvecmat{I}(1,:) - classvecmat{I}(2,:);
    eB = classvecmat{I+1}(1,:) - classvecmat{I+1}(2,:);
    diff = eA.^2-eB.^2;
    meandiff = mean(diff);
    L = length(diff);
    niter = 199;
    som = 0;
    for Q = 1:niter
        randomsign = 2*round(rand(1,L)) - 1;
        signeddiff = randomsign.*diff;
        meansigneddiff = mean(signeddiff);
        som = som + (abs(meansigneddiff)>=abs(meandiff));
    end
    pvalue(I) = (som+1)/(niter+1);
    
end


%check for increasing rmsecv
for iter = 1:MaxLV
    RMSEvec(iter) = rmse_Jan(classvecmat{iter}(1,:), classvecmat{iter}(2,:));
end

for iter = 1:(MaxLV-1)
    signdiff(iter) = sign(RMSEvec(iter)-RMSEvec(iter+1));
end

NumLV_voet = find(pvalue>pvaluevoet, 1);
NumLV_sign = find(signdiff<0, 1);

if isempty(NumLV_voet)
    NumLV_voet = MaxLV;
end

if isempty(NumLV_sign)
    NumLV_sign = MaxLV;
end

NumLV = min(NumLV_voet, NumLV_sign);

%Now, we want to find the corresponding accuracy
Accur = rmse_Jan(classvecmat{NumLV}(1,:), classvecmat{NumLV}(2,:));


