function varargout = pretreat(command,varargin)
% Performs various methods of data pretreatment
%
% Input:
% Command: 'deriv' = first derivative rows data
%           (Input: 'ysmooth', 'nsmooth' to smooth derivative or not
%                    x = data matrix)
%          'deriv2' = second derivative rows data
%           (Input: 'ysmooth', 'nsmooth' to smooth derivative or not
%                    x = data matrix)
%          'smooth1' = Convolution-based boxcar smooth
%           (Input: x = data matrix
%                   w = smoothing window
%          'savgol' = Savitsky-Golay smoothing and differentiation
%          (Input:  y = data matrix
%                   width = smoothing window
%                   order = order of polynomal
%                   deriv = the derivative (set to 0 for only smoothing!)
%          'mc' = mean center data columnwise
%           (Input: 'train', 'test' train or test data
%                    varargin{1} = data matrix
%                    varargin{2} = vector with column means training data)
%          'as' = auto scale data columnwise
%           (Input: 'train', 'test' train or test data
%                    varargin{1} = data matrix
%                    varargin{2} = vector with column means training data
%                    varargin{3} = vector with column standard deviation
%                    training data)
%          'medc' = robustly center data columnwise
%           (Input: 'train', 'test' train or test data
%                    varargin{1} = data matrix
%                    varargin{2} = vector with columns median value from
%                    training data)
%          'snv' = Standard Normal Variate (SNV) transform
%          (Input: x = data matrix)
%          'rnv' = Robust Normal Variate (RNV) transform
%          (Input: x = data matrix
%                  perc = percentile of intensities to use)
%          'msc' = multiplicative scatter correction
%          (Input: x = training data
%                  first = first variable for the correction
%                  last = last variable for the correction
%                  xt = test data)
%          'means' = Scale by mean spectrum
%          (Input: x = data)
%          'meds' = Scale by median spectrum
%          (Input: x = data)
%          'mins' = Scale by min intensity in spectrum
%          (Input: x = data)
%          'maxs' = Scale by max intensity spectrum
%          (Input: x = data)
%          'norms' = Scale spectrum by L2 norm
%          (Input: x = data)

if nargin==1
    error('myApp:argChk', 'Wrong number of input arguments (min = 2)')
else
    switch lower(command)
        case{'as'}
            varargout{:} = as(varargin{:});
        case {'deriv'}
            varargout{:} = deriv(varargin{:});
        case {'deriv2'}
            varargout{:} = deriv2(varargin{:});
        case {'norms'}
            varargout{:} = l2norm(varargin{:});
        case{'medc'}
            varargout{:} = medc(varargin{:});
        case{'meds'}
            varargout{:} = meds(varargin{:});
        case{'means'}
            varargout{:} = means(varargin{:});
        case{'mins'}
            varargout{:} = mins(varargin{:});
        case{'maxs'}
            varargout{:} = maxs(varargin{:});
        case{'mc'}
            varargout{:} = mc(varargin{:});
        case{'msc'}
            varargout{:} = msc(varargin{:});
        case {'savgol'}
            varargout{:} = savgol(varargin{:});
        case {'smooth1'}
            varargout{:} = bsmooth(varargin{:});
        case{'snv'}
            varargout{:} = snv(varargin{:});
        case{'rnv'}
            varargout{:} = snv(varargin{:});
        otherwise
            error('myApp:argChk', 'Not a valid command')
    end
end

function dx = deriv(smooth, x)
% First derivative of data matrix x row wise using a 3-point central
% difference and optional smoothing
%
% Input:
% smooth = 'ysmooth' to smooth the derivative with a triangular weighted
%           smooth function with a windowsize of npx units.
%          'nsmooth' no smoothing of the derivative
% varargin{1} = data matrix x
%
% Output:
% dx = first derivative of the rows in data matrix x

if nargin == 1
    error('myApp:argChk', 'Wrong number of input arguments (min = 2)')
else 
    switch lower(smooth)
        case('ysmooth')
            [n m]=size(x);
            dx = zeros(n,m);
            for i = 1:n
                dx(i,1)=x(i,2)-x(i,1);
                dx(i,m)=x(i,m)-x(i,m-1);
                for j = 2:m-1
                    dx(i,j)=(x(i,j+1)-x(i,j-1))/2;
                end
                if i == 1
                   figure;
                   plot(dx(1,:));
                   disp('What is the average peak width?');
                   pw=input('');
                   disp('What smooth ratio between 0 and 1 would you like to use?');
                   sr=input('');
                   smoothwidth = sr*pw;
                end
                   dx(i,:) = bsmooth(bsmooth(dx(i,:),smoothwidth),smoothwidth);
                   %OF: y = sgolayfilt(varargout,3,5,[],2)!!!!
            end
        case('nsmooth')
            [n m]=size(x);
            dx = zeros(n,m);
            for i = 1:n
                dx(i,1)=x(i,2)-x(i,1);
                dx(i,m)=x(i,m)-x(i,m-1);
                for j = 2:m-1
                    dx(i,j)=(x(i,j+1)-x(i,j-1))/2;
                end
            end
        otherwise
            error('myApp:argChk', 'Wrong number of input arguments')
    end
end

function d2x = deriv2(smooth, x)
% Second derivative of data matrix x row wise using a 3-point central
% difference and optional smoothing
%
% Input:
% smooth = 'ysmooth' to smooth the derivative with a ... 
%          'nsmooth' no smoothing of the derivative
%
% Output:
% dx = second derivative of the rows in data matrix x

if nargin == 1
    error('myApp:argChk', 'Wrong number of input arguments (min = 2)')
else 
    switch lower(smooth)
        case('ysmooth')
            [n m]=size(x);
            d2x = zeros(n,m);
            for i = 1:n
                for j = 2:m-1
                    d2x(i,j)=x(i,j+1)-2*x(i,j)+x(i,j-1);
                end
                d2x(i,1) = d2x(i,2);
                d2x(i,m) = d2x(i,m-1);
                if i == 1
                    figure;
                    plot(d2x(1,:));
                    disp('What is the average peak width?');
                    pw=input('');
                    disp('What smooth ratio between 0 and 1 would you like to use?');
                    sr=input('');
                    smoothwidth = sr*pw;
                end
                d2x(i,:) = bsmooth(bsmooth(bsmooth(d2x(i,:),smoothwidth),smoothwidth),smoothwidth);                
            end
        case('nsmooth')
            [n m]=size(x);
            d2x = zeros(n,m);
            for i = 1:n
                for j = 2:m-1
                    d2x(i,j)=x(i,j+1)-2*x(i,j)+x(i,j-1);
                end
                d2x(i,1) = d2x(i,2);
                d2x(i,m) = d2x(i,m-1);
            end
        otherwise
            error('myApp:argChk', 'Wrong number of input arguments')
    end
end

function s=bsmooth(x,w)
%  Convolution-based boxcar smooth
%  bsmooth(a,w) smooths vector a by a boxcar (rectangular window) of width w
%  T. C. O'Haver, 1988.
% Edited by: Jasper Engel
if nargin == 1
    error('myApp:argChk', 'Wrong number of input arguments')
elseif nargin >=3 
    error('myApp:argChk', 'Wrong number of input arguments')
else 
    [n,~]=size(x);
    for i=1:n
        v=ones(1,w); %define smoothing window
        cx=conv(x(i,:),v); %convolute window with data vector (sums data points within window)
        sp=round((length(v)+1)/2); %start point
        ep=round(length(x(i,:))+sp-1); %end point
        s(i,:)=cx(sp:ep)/w; %average
    end
end

function varargout=mc(set, varargin)
%   Mean centers the data matrix x column wise
%   
%   Input:
%   set = 'train' or 'test'
%   varargin{1} = data matrix
%   varargin{2} = vector with column means training data
%   Output:
%   varargout{1} = mean centered data
%   varargout{2} = vector with column means data

if nargin == 1
    error('myApp:argChk', 'Wrong number of input arguments (min = 2)')
else
    switch lower(set) 
        case {'train'}
            if nargin ~= 2
               error('myApp:argChk', 'Wrong number of input arguments') 
            else
                [n,~]=size(varargin{1});
                %Calculate mean, and center data 
                meandata=mean(varargin{1});
                mcData=varargin{1}-repmat(meandata,n,1);
                Output = struct('mcData',mcData,'mean',meandata);
            end
        case {'test'}
            if nargin ~= 3
               error('myApp:argChk', 'Wrong number of input arguments') 
            else
                [n,~]=size(varargin{1});
                %Center data
                mcData=varargin{1}-repmat(varargin{2},n,1);
                meandata = [];
                Output = struct('mcData',mcData,'mean',meandata);
            end
        otherwise
            error('myApp:argChk', 'Wrong number of input arguments')
    end
end
varargout{1} = Output;

function varargout=as(set, varargin)
%   Auto scales the data matrix x column wise
%   
%   Input:
%   set = 'train' or 'test'
%   varargin{1} = data matrix
%   varargin{2} = vector with column means training data
%   varargin{3} = vector with column standard deviations training data
%   Output:
%   varargout{1} = auto scaled data
%   varargout{2} = vector with column means data
%   varargout{3} = vector with column standard deviations data

if nargin == 1
    error('myApp:argChk', 'Wrong number of input arguments (min = 2)')
else
    switch lower(set) 
        case {'train'}
            if nargin ~= 2
               error('myApp:argChk', 'Wrong number of input arguments') 
            else
                [n,~]=size(varargin{1});
                %Auto scale data 
                meandata=mean(varargin{1});
                mcX=varargin{1}-repmat(meandata,n,1);
                stddata=std(varargin{1});
                asData = mcX./repmat(stddata,n,1);                    
                asData(isnan(asData)) = 0;
                asData(isinf(asData)) = 0;
                Output = struct('asData',asData,'mean',meandata,'std',stddata);
            end
        case {'test'}
            if nargin ~= 4
               error('myApp:argChk', 'Wrong number of input arguments') 
            else
                [n,~]=size(varargin{1});
                %Auto scale data 
                mcX=varargin{1}-repmat(varargin{2},n,1);
                asData = mcX./repmat(varargin{3},n,1);                    
                asData(isnan(asData)) = 0;
                asData(isinf(asData)) = 0;
                meandata = [];
                stddata = [];
                Output = struct('asData',asData,'mean',meandata,'std',stddata);                
            end
        otherwise
            error('myApp:argChk', 'Wrong number of input arguments')
    end
end
varargout{1} = Output;

function varargout=medc(set, varargin)
%   Robustly centers data matrix x columnwise
%   
%   Input:
%   set = 'train' or 'test'
%   varargin{1} = data matrix
%   varargin{2} = vector with columns median value from training data
%   Output:
%   varargout{1} = median centered data
%   varargout{2} = vector with columns median value

if nargin == 1
    error('myApp:argChk', 'Wrong number of input arguments (min = 2)')
else
    switch lower(set)
        case {'train'}
            if nargin ~= 2
                error('myApp:argChk', 'Wrong number of input arguments')
            else
                [n,~]=size(varargin{1});
                median = nanmedian(varargin{1});
                medcData=varargin{1}-repmat(median,n,1);
                Output = struct('medcData',medcData,'median',median);
            end
        case {'test'}
            if nargin ~= 3
                error('myApp:argChk', 'Wrong number of input arguments')
            else
                [n,~]=size(varargin{1});
                median = [];
                medcData = varargin{1}-repmat(varargin{2},n,1);
                Output = struct('medcData',medcData,'median',median);
            end
        otherwise
            error('myApp:argChk', 'Wrong number of input arguments')
    end
end

varargout{1} = Output;

function xMeans = means(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmean = mean(x,2);
xMeans = x./repmat(Xmean,1,m);

function xMins = mins(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmin = min(x,[],2);
xMins = x./repmat(Xmin,1,m);

function xMaxs = maxs(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmax = max(x,[],2);
xMaxs = x./repmat(Xmax,1,m);

function xMeds = meds(x)

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
median = nanmedian(x,2);
xMeds = x./repmat(median,1,m);

function xNorm = l2norm(x)

[n,m]=size(x);
Output = zeros(n,m);
for i=1:n
    xNorm(i,:) = x(i,:)/norm(x(i,:));
end

function xSNV = snv(x)
%   SNV transforms data matrix x row wise
%   
%   Input:
%   x = data matrix
%   
%   Output:
%   xSNV = SNV transformed data matrix

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[~,m]=size(x);
Xmean=mean(x,2);
Xstd=std(x,0,2);
xSNV = (x-repmat(Xmean,1,m))./repmat(Xstd,1,m);

function xRNV = rnv(x, perc)
%   RNV transforms data matrix x row wise
%   
%   Input:
%   x = data matrix
%   perc = percentile (between 0-1) of intensities to use in the computation
%       Often 10 or 25 percentile is used.
%   
%   Output:
%   xRNV = RNV transformed data matrix
%
%   Q.Guo et al. Analytica Chimica Acta (1999) 87-103

if nargin ~= 1
    error('myApp:argChk', 'Wrong number of input arguments (1)')
end

[n,m]=size(x);
[~, x_sort]=sort(x,2);
x_sort = x_sort(:,1:round(m*perc));
Xmean = zeros(n,1); Xstd=zeros(n,1);
for i=1:n
    Xmean(i,1)=mean(x(i,x_sort(i,:)),2);
    Xstd(i,1)=std(x(i,x_sort(i,:)),0,2);
end
xRNV = (x-repmat(Xmean,1,m))./repmat(Xstd,1,m);

function [y_hat,D]= savgol(y,width,order,deriv)
%SAVGOL Savitsky-Golay smoothing and differentiation.
%  Inputs are the matrix of ROW vectors to be smoothed (y),
%  and the optional variables specifying the number of points in
%  filter (width), the order of the polynomial (order), and the
%  derivative (deriv). The output is the matrix of smoothed
%  and differentiated ROW vectors (y_hat) and the matrix of 
%  coefficients (cm) which can be used to create a new smoothed/
%  differentiated matrix, i.e. y_hat = y*cm. If number of points,
%  polynomial order and derivative are not specified,
%  they are set to 15, 2 and 0, respectively.
%
%  Example: if y is a 5 by 100 matrix then savgol(y,11,3,1)
%  gives the 5 by 100 matrix of first-derivative row vectors
%  resulting from a 11-point cubic Savitzky-Golay smooth of
%  each row of y.
%
%I/O: [y_hat,cm] = savgol(y,width,order,deriv);
%
%See also: MSCORR, SAVGOLCV, SGDEMO, STDFIR, BASELINE, LAMSEL


% Sijmen de Jong Unilever Research Laboratorium Vlaardingen Feb 1993
% Modified by Barry M. Wise 5/94
%         ***   Further modified, 1998-03, Martin Andersson
%         ***   Adjusting the calcn. of the bulk data.
%         ***   Based on calcn. of a sparse derivative matrix (D)

[~,n] = size(y);
y_hat = y;
% set default values: 15-point quadratic smooth
if nargin<4
    deriv= 0;
    disp('  '), disp('Derivative set to zero')
end
if nargin<3
    order= 2;
    disp('  '), disp('Polynomial order set to 2')
end
if nargin<2
    width=min(15,floor(n/2));
    s = sprintf('Width set to %g',width);
    disp('  '), disp(s)
end
% In case of input error(s) set to reasonable values
w = max( 3, 1+2*round((width-1)/2) );
if w ~= width
    s = sprintf('Width changed to %g',w);
    disp('  '), disp('Width must be >= 3 and odd'), disp(s)
end
o = min([max(0,round(order)),5,w-1]);
if o ~= order
    s = sprintf('Order changed to %g',o); disp('  ')
    disp('Order must be <= width -1 and <= 5'), disp(s)
end
d = min(max(0,round(deriv)),o);
if d ~= deriv
    s = sprintf('Derivative changed to %g',d); disp('  ')
    disp('Deriviative must be <= order'), disp(s)
end
p = (w-1)/2;
% Calculate design matrix and pseudo inverse
x = ((-p:p)'*ones(1,1+o)).^(ones(size(1:w))'*(0:o));
weights = x\eye(w);
% Smoothing and derivative for bulk of the data
coeff=prod(ones(d,1)*[1:o+1-d]+[0:d-1]'*ones(1,o+1-d,1),1);
D=spdiags(ones(n,1)*weights(d+1,:)*coeff(1),p:-1:-p,n,n);
% Smoothing and derivative for tails
w1=diag(coeff)*weights(d+1:o+1,:);
D(1:w,1:p+1)=[x(1:p+1,1:1+o-d)*w1]';
D(n-w+1:n,n-p:n)=[x(p+1:w,1:1+o-d)*w1]';
% Operate on y using the filtering/derivative matrix, D
y_hat=y*D;

function [xmsc,me,xtmsc]=msc(command,x,first,last,xt)
%  function [xmsc,me,xtmsc]=msc(x,first,last,xt)			 
%									 
%  AIM: 	Multiple Scatter Correction:				 
%		To remove the effect of physical light scatter 		 
%		from the spectrum. (Compensation for particle size	 
%		effects.)						 
%								 
%  PRINCIPLE:  Each spectrum is shifted and rotated so that it fits 	 
%              as closely as possible to the mean spectrum of the data. 
%		The fit is achieved by LS (first-degree polynomial).	 
%		The correction depends on the mean spectrum of the 	 
%		training set.						 
% 									 
%  INPUT:	x: (m x n) matrix with m spectra and n variables	 
%		first: first variable used for correction		 
%		last: last variable used for correction			 
%		 (A segment is selected which is representative for the	 
%		 baseline of the spectra.)				 
%		xt: (mt x nt) matrix for new data (optional)		 
%			 						 
%  OUTPUT:	xmsc: (m x n) matrix containing the spectra after	 
%		    	  correction with msc				 
%		me: mean spectrum (1 x n) of x				 
%		xtmsc: (mt x nt) matrix containing the new spectra after 
%			  correction with msc							 
%								 
%  AUTHOR: 	Andrea Candolfi				 	 
%	    	Copyright(c) 1997 for ChemoAC				 
%          	FABI, Vrije Universiteit Brussel            		 
%          	Laarbeeklaan 103 1090 Jette				 
%    	    								 
% VERSION: 1.1 (28/02/1998)						 
%									 
%  TEST:   	Roy de Maesschalck, Menghui Zhang (2002)	
 
if nargin==2; 
   first=input('The first variable for the correction: '); 
   last=input('The last variables for the correction: '); 
end 
 
[m,n]=size(x); 
switch command
    case{'mean'}
        me=mean(x); 
    case{'median'}
        me=median(x);
    otherwise
        error('myApp:argChk', 'Not a valid command')
end
 
for i=1:m,							% for the x data 
  p=polyfit(me(first:last),x(i,first:last),1);			% least square fit between mean spectrum and each spectrum (first-degree polynomial) 
  xmsc(i,:)=(x(i,:)-p(2)*ones(1,n))./(p(1)*ones(1,n));		% each spectrum is corrected 
end 
 
if nargin ==4;							% correction of new data by using the mean spectrum from x. 
[mt,nt]=size(xt);			 
  for i=1:mt, 
    p=polyfit(me(first:last),xt(i,first:last),1);		% least square fit between mean spectrum and each new spectrum (first-degree polynomial) 
    xtmsc(i,:)=(xt(i,:)-p(2)*ones(1,n))./(p(1)*ones(1,n));	% each new spectrum is corrected 
  end 
end 
 








