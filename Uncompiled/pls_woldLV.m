function [Accur, NumLV, classvecmat] = pls_woldLV(Datashuf, Labelshuf, Scaling, ...
    MaxLV, testmatrix, l, m, n)

%Function to perform PLS scores, including a cross-validation step
%which selects the optimal number of LVs, based on the lowest prediction
%error. Cross-validation is done based on 'leave-Perc%-out'.


%INPUT
%Data: the raw data
%Labels: labels of raw data
%Perc: percentage for leave-Perc%-out (Perc=0.1, 0.15, ...)
%Scaling: name of the scaling method to be included in the cross-validation
%MaxLV: maximum amount of LVs to consider
%Opts: structure with settings for pls to suppress command-line-output
%l, m, k, n: to store values in the 'foutvector'

%OUTPUT
%Accur: accuracy of the best result
%NumLV: number of LVs belonging to the highest accuracy

%Jan Gerretzen, 2013-01-09

global foutvector
global counter

%-----

%Loop
for iter = 1:size(testmatrix,1) %iter: amount of train-test-divisions
    DataTest = Datashuf(testmatrix(iter,1):testmatrix(iter,2), :);
    LabelTest = Labelshuf(testmatrix(iter,1):testmatrix(iter,2));
    DataTrain = Datashuf;
    DataTrain(testmatrix(iter,1):testmatrix(iter,2), :) = [];
    LabelTrain = Labelshuf;
    LabelTrain(testmatrix(iter,1):testmatrix(iter,2)) = [];
    
    %scale train data and test data with parameters from train data
    [DataTrainSc, DataTestSc] = pretreat_scale(Scaling, DataTrain, ...
        DataTest, [], 0.03);
    
    %Check for NaNs of Infs in scaled data
    if (sum(isnan(DataTrainSc(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        Accur=40;
        NumLV=40;
        classvecmat = NaN;
        return
    end
    if (sum(isinf(DataTrainSc(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        Accur=40;
        NumLV=40;
        classvecmat = NaN;
        return
    end
    if (sum(isnan(DataTestSc(:)))~= 0)
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        Accur=40;
        NumLV=40;
        classvecmat = NaN;
        return
    end
    if (sum(isinf(DataTestSc(:)))~= 0) 
        foutvector(counter,:) = [l, m, n];
        counter=counter+1;
        Accur=40;
        NumLV=40;
        classvecmat = NaN;
        return
    end
    
    for iterlv = 1:MaxLV
    B = pls_Jan(DataTrainSc, LabelTrain, iterlv);
    Concpred = (DataTestSc * B(2:end)) + B(1);    
    classvecmat{iterlv}(1,testmatrix(iter,1):testmatrix(iter,2)) = LabelTest;
    classvecmat{iterlv}(2,testmatrix(iter,1):testmatrix(iter,2)) = Concpred;
    %Perc_correct = sum((compmat(:,1)-compmat(:,2))==0);
    %accurmat(iterlv, iter) = Perc_correct;

    clear class
    clear compmat
    clear Perc_correct
    end
    
    clear DataTest
    clear LabelTest
    clear DataTrain
    clear LabelTrain
    clear DataTrainSc
    clear DataTestSc
    clear model
    clear DataTrainScore
    clear pred
    clear DataTestScore
end

%Selection of optimal no. of LVs, based on Wold's adjusted R (R_adj < 0.98)


%check for increasing rmsecv
for iter = 1:MaxLV
    RMSEvec(iter) = rmse_Jan(classvecmat{iter}(1,:), classvecmat{iter}(2,:));
end

stopcrit = 0;
[~, LV] = min(RMSEvec(1:MaxLV-1));

while stopcrit == 0
    R_adj = RMSEvec(LV+1)/RMSEvec(LV);
    if R_adj < 0.98
        NumLV = LV+1; %this appears wrong in Andries paper, if you take LV, then you may have a very large RMSEvec...
        %Suppose that RMSECV(8, 9, 10) = [20, 0.10, 0.11]. You would want
        %to stop at 0.10 (i.e. 9 LVs), but if you take LV, you would stop
        %at 8 LVs with RMSECV 20!
        stopcrit = 1;
    else
        LV = LV - 1;
        if LV == 0
            NumLV = 1;
            stopcrit = 1;
        end
    end
end

%Now, we want to find the corresponding accuracy
Accur = rmse_Jan(classvecmat{NumLV}(1,:), classvecmat{NumLV}(2,:));


