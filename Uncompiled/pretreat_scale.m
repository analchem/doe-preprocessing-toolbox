function [PTtrain, PTtest] = pretreat_scale(method, Xtrain, Xtest, VAIST_penal, perc_offset)
% Data scaling methods
% See: R.A. van den Berg, H.C.J. Hoefsloot, J.A. Westerhuis, A.K. Smilde
% and M.J. van der Werf; BMC Genomics (2006), 7:142
%
% Options:
% MC - meancetering
% AS - autoscaling
% Range - range scaling
% Pareto - pareto scaling
% Poisson - Poisson scaling 
% VAST - Vast scaling
% VAIST - inverse of Vast scaling (VAIST_penal weights the importance of
% std, default = 0; <0 = less important)
% Level - Level scaling
% Log - log transformation
% Logmed - logfold change vs median
% Power - power transformation (square root)
%
% For AS, Range, Poisson and Level an offset term can be given to reduce the influence
% of variables with mean or std close to zero; usually 1-5% of the maximum of all variable values is used. 
%
% input VAIST_penal = optional
%
%
% Sept 2012 - Jasper Engel



if nargin==1
    error('myApp:argChk', 'Wrong number of input arguments (min = 2)')
else
    
    [n_train,~]=size(Xtrain);
    if isempty(Xtest)
    else
    [n_test,~]=size(Xtest);
    end
    
    switch method
        case 'None' % Do nothing
            PTtrain = Xtrain;
            PTtest = Xtest;
            
        case 'MC' % Meancenter data - focus on the differences in the data and not on similarities
            meandata=mean(Xtrain);
            PTtrain=Xtrain-repmat(meandata,n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else 
                PTtest = Xtest - repmat(meandata,n_test,1);
            end
            
        case 'AS' % Autoscaling - Compare metabolites based on correlations (all metabolites become equally important)
            meandata = mean(Xtrain);
            stddata = std(Xtrain);
            
            offset = perc_offset*max(stddata);
            
            PTtrain = (Xtrain - repmat(meandata,n_train,1))./repmat((stddata + offset),n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else
                PTtest = (Xtest-repmat(meandata,n_test,1))./repmat((stddata + offset),n_test,1);
            end
        case 'Range' % Range scaling - Compare metabolites relative to the biological response range (Scaling is related to biology, but sensitive to outliers)
            meandata = mean(Xtrain);
            rangedata = max(Xtrain,[],1)-min(Xtrain,[],1);
            
            offset = perc_offset * max(rangedata);
            
            PTtrain = (Xtrain - repmat(meandata,n_train,1))./repmat((rangedata + offset),n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else
                PTtest = (Xtest-repmat(meandata,n_test,1))./repmat((rangedata + offset),n_test,1);
            end
        case 'Poisson'
            diff = 10 - min(min(Xtrain));  % Otherwise Poisson scaling can divide by a negative root --> complex numbers
            Xtrain = Xtrain + diff;
            Xtest = Xtest + diff;
            
            meandata = mean(Xtrain);
            
            offset = perc_offset*max(meandata);
            
            PTtrain = (Xtrain - repmat(meandata,n_train,1))./repmat(sqrt(meandata + offset),n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else 
                PTtest = (Xtest - repmat(meandata,n_test,1))./repmat(sqrt(meandata + offset),n_test,1);
            end
            
            
        case 'Pareto' % Pareto scaling - Reduce the relative importance of large values, but keep data structure partially intact (Sensitive to large fold changes)
            meandata = mean(Xtrain);
            stddata = sqrt(std(Xtrain));
            
            offset = perc_offset*max(stddata);
            
            PTtrain = (Xtrain - repmat(meandata,n_train,1))./repmat(stddata + offset,n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else
                PTtest = (Xtest-repmat(meandata,n_test,1))./repmat(stddata + offset,n_test,1);
            end
        case 'VAST' % Apply VAST scaling - Focus on the metabolites that show small fluctuations (Aims for robustness)
            meandata = mean(Xtrain);
            stddata = std(Xtrain);
            
            CfVar = stddata./meandata;
            
            PTtrain = (Xtrain - repmat(meandata,n_train,1))./repmat(stddata,n_train,1).*repmat(1./CfVar,n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else
                PTtest = (Xtest-repmat(meandata,n_test,1))./repmat(stddata,n_test,1).*repmat(1./CfVar,n_test,1);
            end
            
        case 'VAIST' % Apply instability scaling - Focus on the metabolites that show the largest fluctuations
            if isempty(VAIST_penal)
                instability_penalty = 1;
            else
                instability_penalty = VAIST_penal;
            end
            
            meandata = mean(Xtrain);
            stddata = std(Xtrain);
            
            CfVar = stddata.^instability_penalty./meandata;
            
            PTtrain = (Xtrain - repmat(meandata,n_train,1))./repmat(stddata,n_train,1).*repmat(CfVar,n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else
                PTtest = (Xtest-repmat(meandata,n_test,1))./repmat(stddata,n_test,1).*repmat(CfVar,n_test,1);
            end
            
        case 'Level'   % Level scaling - Focus on relative response (Suited for identification of e.g. biomarkers)
            meandata = mean(Xtrain);
            
            offset = perc_offset * max(meandata);
            
            PTtrain = (Xtrain - repmat(meandata,n_train,1))./repmat((meandata + offset),n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else
                PTtest = (Xtest-repmat(meandata,n_test,1))./repmat((meandata + offset),n_test,1);
            end
        case 'Log' % Log transformation - Correct for heteroscedasticity, make multiplicative models additive. (Difficulties with values with large relative SD and zeros)
            
            % To make sure the log transform doesnt have to deal with
            % zeros; this transformation would be strange if the data was
            % already centered!!
            diff = 10 - min(min(Xtrain));
            Xtrain = Xtrain + diff;
            Xtest = Xtest + diff;
                     
            logtrain = log10(Xtrain);
            meandata = mean(logtrain);
            PTtrain=logtrain-repmat(meandata,n_train,1);
            if isempty(Xtest)
                PTtest = [];
            else
                logtest = log10(Xtest);
                PTtest=logtest-repmat(meandata,n_test,1);
            end
        case 'Logmed' %Log2-fold change vs. median
            
            diff = 10 - min(min(Xtrain));
            Xtrain = Xtrain + diff;
            Xtest = Xtest + diff;
            
            median_X = median(abs(Xtrain));
            FC_train = Xtrain./repmat((median_X),n_train,1);
            logtrain = log2(FC_train); %10log zou eventueel ook kunnen
            meandata = mean(logtrain);
            PTtrain=logtrain-repmat(meandata,n_train,1);
            
            if isempty(Xtest)
                PTtest = [];
            else 
                FC_test = Xtest./repmat((median_X),n_test,1);
                logtest = log2(FC_test);
                PTtest=logtest-repmat(meandata,n_test,1);
            end

        case 'Power' % Power transformation - Correct for heteroscedasticity (No problems with small values)
            sqrttrain = sqrt(Xtrain);
            meandata = mean(sqrttrain);
            PTtrain=sqrttrain-repmat(meandata,n_train,1);
            if isempty(Xtest)
                PTtest = [];
            else
                sqrttest = sqrt(Xtest);
                PTtest=sqrttest-repmat(meandata,n_test,1);
            end
    end
end



