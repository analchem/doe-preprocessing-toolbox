function varargout = Doe_Toolbox_V2(varargin)
% DOE_TOOLBOX_V2 MATLAB code for Doe_Toolbox_V2.fig
%      DOE_TOOLBOX_V2, by itself, creates a new DOE_TOOLBOX_V2 or raises the existing
%      singleton*.
%
%      H = DOE_TOOLBOX_V2 returns the handle to a new DOE_TOOLBOX_V2 or the handle to
%      the existing singleton*.
%
%      DOE_TOOLBOX_V2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DOE_TOOLBOX_V2.M with the given input arguments.
%
%      DOE_TOOLBOX_V2('Property','Value',...) creates a new DOE_TOOLBOX_V2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Doe_Toolbox_V2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Doe_Toolbox_V2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Note GJ Postma 8/2017: with for instance the wheat data the first
% scatter correction meathod means.m (called by preprocess_St.m, line 254) 
% (which divides the data by the row-means) can produce very large values 
% (10^12, 10^13) because that method divides by the row-means of the data  
% and those row-means can approach zero (in fact: 10^-16 - 10^-17). One
% should not divide by such small values, so in that case the first
% scatter correction method should be skipped. This should be
% implemented in future updates.
        
% Edit the above text to modify the response to help Doe_Toolbox_V2

% Last Modified by GUIDE v2.5 14-May-2020 18:20:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Doe_Toolbox_V2_OpeningFcn, ...
                   'gui_OutputFcn',  @Doe_Toolbox_V2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Doe_Toolbox_V2 is made visible.
function Doe_Toolbox_V2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Doe_Toolbox_V2 (see VARARGIN)

% Choose default command line output for Doe_Toolbox_V2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Doe_Toolbox_V2 wait for user response (see UIRESUME)
% uiwait(handles.DoE_figure);

addpath('pp_methods\')
warning('off', 'all') %such that no warnings are displayed when using detrending

%Initialize starting parameters
setappdata(handles.DoE_figure,'MaxLV',20);
setappdata(handles.DoE_figure, 'AsLS_param', [1000000 0.001]);
setappdata(handles.DoE_figure, 'Savgol_B', 11);
setappdata(handles.DoE_figure, 'Savgol_Sm_window', [5 9 11]);
setappdata(handles.DoE_figure, 'Savgol_Sm_order', [2 3 4]);
setappdata(handles.DoE_figure, 'CV_perc', 0.1);

%Initialize title
set(handles.DoE_figure, ...
    'Name',sprintf('Preprocessing Toolbox - Jan Gerretzen, 2016, Radboud University.'), ...
    'NumberTitle','off');

%Initialize logos
[im, map, alpha] = imread('coast_logo.png');
set(handles.DoE_figure,'CurrentAxes',handles.coast_logo);
hold on
f = imshow(im);
set(f, 'AlphaData', alpha)
[im, map, alpha] = imread('ru_logo.png');
set(handles.DoE_figure,'CurrentAxes',handles.ru_logo);
hold on
f = imshow(im);
set(f, 'AlphaData', alpha)

%Deactivate buttons that you should not yet press:
set(handles.pushbutton_CalculateDoE, 'Enable', 'Off');
set(handles.pushbutton_Optimize, 'Enable', 'Off');
set(handles.pushbutton_save_data_matlab, 'Enable', 'Off');
set(handles.pushbutton_save_data_excel, 'Enable', 'Off');
set(handles.pushbutton_save_data_csv, 'Enable', 'Off');
set(handles.pushbutton_preprocess, 'Enable', 'Off');
set(handles.pushbutton_MakeReport, 'Enable', 'Off');


% --- Outputs from this function are returned to the command line.
function varargout = Doe_Toolbox_V2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton_Optimize.
function pushbutton_Optimize_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Optimize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Load data
Xtrain = getappdata(handles.DoE_figure, 'Xtrain');
Xtest = getappdata(handles.DoE_figure, 'Xtest');
Ytrain = getappdata(handles.DoE_figure, 'Ytrain');
Ytest = getappdata(handles.DoE_figure, 'Ytest');

%Initialize default parameters
Scaling = {'MC', 'AS', 'Range', 'Pareto', 'Poisson', 'Level', 'Log'};

%Read parameters
AsLS_param = getappdata(handles.DoE_figure, 'AsLS_param');
Savgol_B = getappdata(handles.DoE_figure, 'Savgol_B');
win = getappdata(handles.DoE_figure, 'Savgol_Sm_window');
ord = getappdata(handles.DoE_figure, 'Savgol_Sm_order');

Smooth = [win(1),win(2),win(3),win(1),win(2),win(3),win(1),win(2),win(3); ...
    ord(1),ord(1),ord(1),ord(2),ord(2),ord(2),ord(3),ord(3),ord(3)];
% Set PR settings
par(1) = 15;
par(2) = 25;
par(3) = 35;
par(5) = 2;
par(6) = 3;
par(7) = 4;
par(8) = AsLS_param(1);
par(9) = AsLS_param(2);
par(10) = 0;
par(11) = 0;
par(12) = Savgol_B;

MaxLV = getappdata(handles.DoE_figure, 'MaxLV'); %maximum number of LVs in PLS model
pvaluevoet = 0.5;

% Obtain settings for variable selection
varsel = get(handles.popupmenu_varselect, 'Value'); %1 = no variable selection, 2 = variable selection

%Construct testmatrix for cross-validation
Perc = getappdata(handles.DoE_figure, 'CV_perc');
[m, ~] = size(Xtrain);
testsize = floor(Perc * m);
remain = mod(m, testsize);

dummy=1;
testmatrix=[];
for q = 1:(floor(m/testsize)-remain)
    testmatrix(q,1)=dummy;
    testmatrix(q,2)=dummy+testsize-1;
    dummy=dummy+testsize;
end

if remain>0
    for q=(floor(m/testsize)-remain+1):floor(m/testsize)
    testmatrix(q,1)=dummy;
    testmatrix(q,2)=dummy+testsize;
    dummy=dummy+testsize+1;
    end
end

clear testsize remain dummy

% Which steps should be optimized?
valB = get(handles.checkbox_B, 'Value');
valSt = get(handles.checkbox_St, 'Value');
valSm = get(handles.checkbox_Sm, 'Value');
valSg = get(handles.checkbox_Sg, 'Value');

sum = valB+valSt+valSm+valSg;

dummy = 1;

h = waitbar(0);

if valB==1    
    % Perform baseline correction
    waitbar(dummy/(sum+1), h, 'Optimizing baseline correction...')
    Xpre_train{1} = Xtrain;
    Xpre_test{1} = Xtest;
    
    for i = 1:9
        Xpre_train{i+1} = preprocess_B(Xtrain, i, par);
        Xpre_test{i+1} =  preprocess_B(Xtest, i, par);
    end
    
    for i=1:10
        if varsel==1
            waitbar(dummy/(sum+1) + (i/12)*(1/(sum+1)), h, 'Optimizing baseline correction...')
            [~, complevecB(i), ~] = plsldagcv_vdvoet_lv_selection(Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), MaxLV, testmatrix, [], [], [], [], pvaluevoet); %optimize no. of LVs
            [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %meancenter training and test set
        else
            waitbar(dummy/(sum+1) + (i/8)*(1/(sum+1)), h, 'Optimizing baseline correction with variable selection...')
            
            [RMSECV_fs(i), complevec_fs(i), ~] = pls_woldLV(Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), MaxLV, testmatrix, [], [], []); %to determine complexity of model without variable selection
        
            [HuhnSc, ~] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %scale data (scaling not required when doing variable selection)
        
            [~, vars_removedB{i}, complevecB(i)] = pprvfcam(HuhnSc, Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), complevec_fs(i), testmatrix, [], [], [], RMSECV_fs(i));
            
            [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %meancenter training and test set
            
            Xpre_train{i}(:, vars_removedB{i}) = [];
            Xpre_test{i}(:, vars_removedB{i}) = [];
        end
        
        %Build PLS model
        [B] = pls_Jan(Xpre_train{i}, Ytrain, complevecB(i));
        pred = Xpre_test{i} * B(2:end) + B(1);
        rmsepvecB(i) = rmse_Jan(Ytest, pred);
    end
    
    [rmsepmin,b] = min(rmsepvecB); %find best baseline correction
    opt_complexity = complevecB(b);
    
    if b~=1
        Xtrain = preprocess_B(Xtrain, b-1, par);
        Xtest = preprocess_B(Xtest, b-1, par);
        stringB = get(handles.popupmenu_B, 'String');
        set(handles.resultB, 'String', stringB(b-1,:));
    else
        set(handles.resultB, 'String', 'none');
    end
    set(handles.popupmenu_ppB, 'Value', b);
    dummy = dummy+1;
    
end



if valSt==1    
    % Perform scatter correction
    waitbar(dummy/(sum+1), h, 'Optimizing scatter correction...')
    Xpre_train{1} = Xtrain;
    Xpre_test{1} = Xtest;
    
    for i = 1:9
        Xpre_train{i+1} = preprocess_St(Xtrain, i, par);
        Xpre_test{i+1} =  preprocess_St(Xtest, i, par);
    end
    
    for i=1:10
        if varsel==1
            waitbar(dummy/(sum+1) + (i/11)*(1/(sum+1)), h, 'Optimizing scatter correction...')
            [~, complevecSt(i), ~] = plsldagcv_vdvoet_lv_selection(Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), MaxLV, testmatrix, [], [], [], [], pvaluevoet); %optimize no. of LVs
            [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %meancenter training and test set
        else
            waitbar(dummy/(sum+1) + (i/8)*(1/(sum+1)), h, 'Optimizing scatter correction with variable selection...')
            
            [RMSECV_fs(i), complevec_fs(i), ~] = pls_woldLV(Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), MaxLV, testmatrix, [], [], []); %to determine complexity of model without variable selection
        
            [HuhnSc, ~] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %scale data (scaling not required when doing variable selection)
        
            [~, vars_removedSt{i}, complevecSt(i)] = pprvfcam(HuhnSc, Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), complevec_fs(i), testmatrix, [], [], [], RMSECV_fs(i));
            
            [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %meancenter training and test set
            
            Xpre_train{i}(:, vars_removedSt{i}) = [];
            Xpre_test{i}(:, vars_removedSt{i}) = [];
        end            
        
        %Build PLS model
        % Note GJ Postma 8/2017: with for instance the wheat data the first
        % scatter correction meathod means.m (which divides the data by the
        % row-means) can produce very large values (10^12, 10^13) because
        % that method divides by the row-means of the data and those
        % row-means can approach zero (in fact: 10^-16 - 10^-17). One
        % should not divide by such small values, so in that case the first
        % scatter correction method should be skipped. This should be
        % implemented in future updates.
        [B] = pls_Jan(Xpre_train{i}, Ytrain, complevecSt(i));
        pred = Xpre_test{i} * B(2:end) + B(1);
        rmsepvecSt(i) = rmse_Jan(Ytest, pred);
    end
    
    [rmsepmin,b] = min(rmsepvecSt); %find best scatter correction
    opt_complexity = complevecSt(b);
    
    if b~=1
        Xtrain = preprocess_St(Xtrain, b-1, par);
        Xtest = preprocess_St(Xtest, b-1, par);
        stringSt = get(handles.popupmenu_St, 'String');
        set(handles.resultSt, 'String', stringSt(b-1,:));
    else
        set(handles.resultSt, 'String', 'none');
    end
    set(handles.popupmenu_ppSt, 'Value', b);
    dummy = dummy+1;
end

if valSm==1    
    % Perform smoothing
    waitbar(dummy/(sum+1), h, 'Optimizing smoothing...')
    Xpre_train{1} = Xtrain;
    Xpre_test{1} = Xtest;
    
    for i = 1:9
        Xpre_train{i+1} = pretreat('savgol', Xtrain, Smooth(1,i), ...
            Smooth(2,i), 0);
        Xpre_test{i+1} = pretreat('savgol', Xtest, Smooth(1,i), ...
            Smooth(2,i), 0);
    end
    
    for i=1:10
        if varsel==1
            waitbar(dummy/(sum+1) + (i/11)*(1/(sum+1)), h, 'Optimizing smoothing...')
            [~, complevecSm(i), ~] = plsldagcv_vdvoet_lv_selection(Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), MaxLV, testmatrix, [], [], [], [], pvaluevoet); %optimize no. of LVs
            [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %meancenter training and test set
        else
            waitbar(dummy/(sum+1) + (i/8)*(1/(sum+1)), h, 'Optimizing smoothing with variable selection...')
            
            [RMSECV_fs(i), complevec_fs(i), ~] = pls_woldLV(Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), MaxLV, testmatrix, [], [], []); %to determine complexity of model without variable selection
        
            [HuhnSc, ~] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %scale data (scaling not required when doing variable selection)
        
            [~, vars_removedSm{i}, complevecSm(i)] = pprvfcam(HuhnSc, Xpre_train{i}, Ytrain, ...
                char(Scaling(1)), complevec_fs(i), testmatrix, [], [], [], RMSECV_fs(i));
            
            [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
                Xpre_test{i}, [], 0.03); %meancenter training and test set
            
            Xpre_train{i}(:, vars_removedSm{i}) = [];
            Xpre_test{i}(:, vars_removedSm{i}) = [];
        end

        
        %Build PLS model
        [B] = pls_Jan(Xpre_train{i}, Ytrain, complevecSm(i));
        pred = Xpre_test{i} * B(2:end) + B(1);
        rmsepvecSm(i) = rmse_Jan(Ytest, pred);
    end
    
    [rmsepmin,b] = min(rmsepvecSm); %find best smoothing
    opt_complexity = complevecSm(b);
    
    if b~=1
        Xtrain = pretreat('savgol', Xtrain, Smooth(1,b-1), ...
            Smooth(2,b-1), 0);
        Xtest = pretreat('savgol', Xtest, Smooth(1,b-1), ...
            Smooth(2,b-1), 0);
        stringSm = get(handles.popupmenu_Sm, 'String');
        set(handles.resultSm, 'String', stringSm(b-1,:));
    else
        set(handles.resultSm, 'String', 'none');
    end
    set(handles.popupmenu_ppSm, 'Value', b);
    dummy = dummy + 1;
end



if valSg==1    
    % Perform scaling
    waitbar(dummy/(sum+1), h, 'Optimizing scaling...')
    
    for i=1:7
        if varsel==1
            waitbar(dummy/(sum+1) + (i/8)*(1/(sum+1)), h, 'Optimizing scaling...')
            [~, complevecSg(i), ~] = plsldagcv_vdvoet_lv_selection(Xtrain, Ytrain, ...
                char(Scaling(i)), MaxLV, testmatrix, [], [], [], [], pvaluevoet); %optimize no. of LVs
            [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(i)), Xtrain, ...
                Xtest, [], 0.03); %meancenter training and test set
        else
            waitbar(dummy/(sum+1) + (i/8)*(1/(sum+1)), h, 'Optimizing scaling with variable selection...')
            
            [RMSECV_fs(i), complevec_fs(i), ~] = pls_woldLV(Xtrain, Ytrain, ...
                char(Scaling(i)), MaxLV, testmatrix, [], [], []); %to determine complexity of model without variable selection
        
            [HuhnSc, ~] = pretreat_scale(char(Scaling(i)), Xtrain, ...
                Xtest, [], 0.03); %scale data (scaling not required when doing variable selection)
        
            [~, vars_removedSg{i}, complevecSg(i)] = pprvfcam(HuhnSc, Xtrain, Ytrain, ...
                char(Scaling(i)), complevec_fs(i), testmatrix, [], [], [], RMSECV_fs(i));
            
            [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(i)), Xtrain, ...
                Xtest, [], 0.03); %meancenter training and test set
            
            Xpre_train{i}(:, vars_removedSg{i}) = [];
            Xpre_test{i}(:, vars_removedSg{i}) = [];
        end

        
        %Build PLS model
        [B] = pls_Jan(Xpre_train{i}, Ytrain, complevecSg(i));
        pred = Xpre_test{i} * B(2:end) + B(1);
        rmsepvecSg(i) = rmse_Jan(Ytest, pred);
    end
    
    [rmsepmin,b] = min(rmsepvecSg); %find best scaling
    opt_complexity = complevecSg(b);
    
    Xtrain = Xpre_train{b};
    Xtest = Xpre_test{b};
    
    if b~=1
        stringSg = get(handles.popupmenu_Sg, 'String');
        set(handles.resultSg, 'String', stringSg(b-1,:));
    else
        set(handles.resultSg, 'String', 'Meancenter');
    end
    set(handles.popupmenu_ppSg, 'Value', b);
end

close(h)

if exist('rmsepmin')
else
    rmsepmin = NaN;
end
set(handles.text_RMSEP, 'String', num2str(rmsepmin))
set(handles.text_no_of_LVs, 'String', num2str(opt_complexity))
assignin('base','Xtrain_pp', Xtrain);
assignin('base','Xtest_pp', Xtest);
setappdata(handles.DoE_figure,'Xtrain_pp',Xtrain);
setappdata(handles.DoE_figure,'Xtest_pp',Xtest);
set(handles.text_status, 'String', 'Sequential optimization: finished!')
drawnow();

%Activate next button:
set(handles.pushbutton_save_data_matlab, 'Enable', 'On');
set(handles.pushbutton_save_data_excel, 'Enable', 'On');
set(handles.pushbutton_save_data_csv, 'Enable', 'On');


% Update figure with selected variables or with preprocessed data

if varsel==1
    set(handles.DoE_figure,'CurrentAxes',handles.effects_plot);
    %make plot of Xtrain and Xtest
    plot(Xtrain', 'k')
    hold on
    plot(Xtest', 'r')
    hold off
    set(handles.text8,'String', 'Xtrain_pp (black) and Xtest_pp (red)')
end

if varsel==2
    ppselection = [valB valSt valSm valSg];
    finalpp = max(find(ppselection==1)); %find last performed preprocessing step
    
    if finalpp==1
        vars_removed = vars_removedB;
    elseif finalpp==2
        vars_removed = vars_removedSt;
    elseif finalpp==3
        vars_removed = vars_removedSm;
    else
        vars_removed = vars_removedSg;
    end
    
    vars_removed = vars_removed{b}; %b always indicates the optimal preprocessing
    
    assignin('base','vars_removed', vars_removed);
    
    set(handles.DoE_figure,'CurrentAxes',handles.effects_plot);
    
    Xtrain = getappdata(handles.DoE_figure, 'Xtrain'); %for plotting purposes
    Xtrain = Xtrain(1,:);
    
    vars = ones(length(Xtrain), 1).*max(Xtrain(1,:));
    vars(vars_removed) = 0; %this is the strategy found by seq. opt
    bar(1:length(Xtrain), vars, 'b')
    hold on
    plot(1:length(Xtrain), Xtrain, 'LineWidth', 2, 'Color', 'k')
    hold off
    xlabel('Variable no.')
    ylabel('Absorbance')
    title('Selected variables (blue)')
    drawnow();
end

        
    
    


% --- Executes on button press in pushbutton_LoadExcel.
function pushbutton_LoadExcel_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_LoadExcel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileName,pathName] = uigetfile('*.xlsx','Load raw data (Excel)');
% Check if cancel pressed
if isequal(fileName,0)
    set(handles.text_status, 'String', 'No data file was selected')
    return
end

set(handles.text_status, 'String', 'Loading data...')
drawnow();

% Read data in excelfile
excelFile=fullfile(pathName,fileName);
try 
    Xtrain=xlsread(excelFile, 'Xtrain');
catch
    message = 'Cannot load Xtrain. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

try 
    Xtest=xlsread(excelFile, 'Xtest');
catch
    message = 'Cannot load Xtest. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

try 
    Ytrain=xlsread(excelFile, 'Ytrain');
catch
    message = 'Cannot load Ytrain. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

try 
    Ytest=xlsread(excelFile, 'Ytest');
catch
    message = 'Cannot load Ytest. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

%Perform some checks of the data
if size(Xtrain,1) ~= size(Ytrain, 1)
    message =  'The number of samples in Xtrain and Ytrain differ! Please check. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

if size(Xtest,1) ~= size(Ytest, 1)
    message =  'The number of samples in Xtest and Ytest differ! Please check. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

if size(Xtest,2) ~= size(Xtrain, 2)
    message =  'The number of variables in Xtrain and Xtest is unequal! Please check. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end
    
setappdata(handles.DoE_figure,'Xtrain',Xtrain);
setappdata(handles.DoE_figure,'Xtest',Xtest);
setappdata(handles.DoE_figure,'Ytrain',Ytrain);
setappdata(handles.DoE_figure,'Ytest',Ytest);
%Reserve memory for preprocessed data:
setappdata(handles.DoE_figure,'Xtrain_pp',Xtrain);
setappdata(handles.DoE_figure,'Xtest_pp',Xtest);

set(handles.DoE_figure,'CurrentAxes',handles.effects_plot);
%make plot of Xtrain and Xtest
p1 = plot(Xtrain', 'k');
hold on
p2 = plot(Xtest', 'r');
hold off
legend([p1(1) p2(1)], {'Xtrain',  'Xtest'}, 'Location', 'Best');
set(handles.text8,'String', 'Raw X data')
set(handles.text_status, 'String', 'Successfully loaded the data')
set(handles.DoE_figure, ...
    'Name',sprintf('Preprocessing Toolbox - ? Jan Gerretzen, 2016, Radboud University. Current data file: %s', fileName), ...
    'NumberTitle','off');

%Activate the next button:
set(handles.pushbutton_CalculateDoE, 'Enable', 'On');
set(handles.pushbutton_preprocess, 'Enable', 'On');


% --- Executes on selection change in popupmenu_B.
function popupmenu_B_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_B contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_B


% --- Executes during object creation, after setting all properties.
function popupmenu_B_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_St.
function popupmenu_St_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_St (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_St contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_St


% --- Executes during object creation, after setting all properties.
function popupmenu_St_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_St (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_Sm.
function popupmenu_Sm_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_Sm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_Sm contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_Sm


% --- Executes during object creation, after setting all properties.
function popupmenu_Sm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_Sm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_Sg.
function popupmenu_Sg_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_Sg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_Sg contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_Sg


% --- Executes during object creation, after setting all properties.
function popupmenu_Sg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_Sg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_CalculateDoE.
function pushbutton_CalculateDoE_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_CalculateDoE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Load data
Xtrain = getappdata(handles.DoE_figure, 'Xtrain');
Xtest = getappdata(handles.DoE_figure, 'Xtest');
Ytrain = getappdata(handles.DoE_figure, 'Ytrain');
Ytest = getappdata(handles.DoE_figure, 'Ytest');

if isempty(Xtrain) || isempty(Xtest) || isempty(Ytrain) || isempty(Ytest)
    message = 'You did not load all files properly, try again!';
    uiwait(warndlg(message));
    return
end


%Initialize default parameters
Scaling = {'MC', 'AS', 'Range', 'Pareto', 'Poisson', 'Level', 'Log'};

%Read parameters
AsLS_param = getappdata(handles.DoE_figure, 'AsLS_param');
Savgol_B = getappdata(handles.DoE_figure, 'Savgol_B');
win = getappdata(handles.DoE_figure, 'Savgol_Sm_window');
ord = getappdata(handles.DoE_figure, 'Savgol_Sm_order');

Smooth = [win(1),win(2),win(3),win(1),win(2),win(3),win(1),win(2),win(3); ...
    ord(1),ord(1),ord(1),ord(2),ord(2),ord(2),ord(3),ord(3),ord(3)];
% Set PR settings
par(1) = 15;
par(2) = 25;
par(3) = 35;
par(5) = 2;
par(6) = 3;
par(7) = 4;
par(8) = AsLS_param(1);
par(9) = AsLS_param(2);
par(10) = 0;
par(11) = 0;
par(12) = Savgol_B;

MaxLV = getappdata(handles.DoE_figure, 'MaxLV'); %maximum number of LVs in PLS model
pvaluevoet = 0.5;


%Construct testmatrix for cross-validation
Perc = getappdata(handles.DoE_figure, 'CV_perc');
[m, ~] = size(Xtrain);
testsize = floor(Perc * m);
remain = mod(m, testsize);

dummy=1;
testmatrix=[];
for q = 1:(floor(m/testsize)-remain)
    testmatrix(q,1)=dummy;
    testmatrix(q,2)=dummy+testsize-1;
    dummy=dummy+testsize;
end

if remain>0
    for q=(floor(m/testsize)-remain+1):floor(m/testsize)
    testmatrix(q,1)=dummy;
    testmatrix(q,2)=dummy+testsize;
    dummy=dummy+testsize+1;
    end
end

clear testsize remain dummy


% Obtain settings for DoE
val_B = get(handles.popupmenu_B, 'Value');
val_St = get(handles.popupmenu_St, 'Value');
val_Sm = get(handles.popupmenu_Sm, 'Value');
val_Sg = get(handles.popupmenu_Sg, 'Value');

% Obtain settings for variable selection
varsel = get(handles.popupmenu_varselect, 'Value'); %1 = no variable selection, 2 = variable selection


% Indices for each preprocessing step: first row is perform the step,
% second row is do not perform the step
ind_B = [1:8; 9:16];
ind_St = [1:4, 9:12; 5:8, 13:16];
ind_Sm = [1 2 5 6 9 10 13 14; 3 4 7 8 11 12 15 16];
ind_Sg = [1 3 5 7 9 11 13 15; 2 4 6 8 10 12 14 16];



h = waitbar(0.2, 'Performing baseline correction...');
% Perform baseline correction
for i = ind_B(1,:)
    Xpre_train{i} = preprocess_B(Xtrain, val_B, par);
    Xpre_test{i} =  preprocess_B(Xtest, val_B, par);
end
for i = ind_B(2,:)
    Xpre_train{i} = Xtrain;
    Xpre_test{i} =  Xtest;
end

waitbar(0.4, h, 'Performing scatter correction...')
%Perform scatter correction
for i = ind_St(1,:)
    Xpre_train{i} = preprocess_St(Xpre_train{i}, val_St, par);
    Xpre_test{i} = preprocess_St(Xpre_test{i}, val_St, par);
end


waitbar(0.6, h, 'Performing smoothing...')
%Perform smoothing
for i = ind_Sm(1,:)
    Xpre_train{i} = pretreat('savgol', Xpre_train{i}, Smooth(1,val_St), ...
        Smooth(2,val_St), 0);
    Xpre_test{i} = pretreat('savgol', Xpre_test{i}, Smooth(1,val_St), ...
        Smooth(2,val_St), 0);
end

%Determine the optimal no. of LVs for each row in the DoE, based on the
%training data set and appropriate scaling
dummy=1;
if varsel == 1 %so no variable selection
    for i=ind_Sg(1,:)
        waitbar(0.8, h, strcat('Scaling and cross-validation (', num2str(dummy),'/16)'))
        [~, complevec_exp(i), ~] = plsldagcv_vdvoet_lv_selection(Xpre_train{i}, Ytrain, ...
            char(Scaling(val_Sg+1)), MaxLV, testmatrix, [], [], [], [], pvaluevoet);    
        dummy = dummy+1;
    end
    for i=ind_Sg(2,:)
        waitbar(0.8, h, strcat('Scaling and cross-validation (', num2str(dummy),'/16)'))
        [~, complevec_exp(i), ~] = plsldagcv_vdvoet_lv_selection(Xpre_train{i}, Ytrain, ...
            char(Scaling(1)), MaxLV, testmatrix, [], [], [], [], pvaluevoet);
        dummy = dummy+1;
    end
else %with variable selection
    for i=ind_Sg(1,:)
        waitbar(0.8, h, strcat('Scaling, variable selection and cross-validation (', num2str(dummy),'/16)'))
        [RMSECV_fs(i), complevec_fs(i), ~] = pls_woldLV(Xpre_train{i}, Ytrain, ...
            char(Scaling(val_Sg+1)), MaxLV, testmatrix, [], [], []); %to determine complexity of model without variable selection
        
        [HuhnSc, ~] = pretreat_scale(char(Scaling(val_Sg+1)), Xpre_train{i}, ...
            Xpre_test{i}, [], 0.03); %scale data (scaling not required when doing variable selection)
        
        [~, vars_removed{i}, complevec_exp(i)] = pprvfcam(HuhnSc, Xpre_train{i}, Ytrain, ...
            char(Scaling(val_Sg+1)), complevec_fs(i), testmatrix, [], [], [], RMSECV_fs(i));
        dummy = dummy + 1;
    end
    for i=ind_Sg(2,:)
        waitbar(0.8, h, strcat('Scaling, variable selection and cross-validation (', num2str(dummy),'/16)'))
        [RMSECV_fs(i), complevec_fs(i), ~] = pls_woldLV(Xpre_train{i}, Ytrain, ...
            char(Scaling(1)), MaxLV, testmatrix, [], [], []); %to determine complexity of model without variable selection
        
        [HuhnSc, ~] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
            Xpre_test{i}, [], 0.03); %scale data (scaling not required when doing variable selection)
        
        [~, vars_removed{i}, complevec_exp(i)] = pprvfcam(HuhnSc, Xpre_train{i}, Ytrain, ...
            char(Scaling(1)), complevec_fs(i), testmatrix, [], [], [], RMSECV_fs(i));
        dummy = dummy + 1;
    end
end  
        
%Perform scaling of training and test set
for i=ind_Sg(1,:)
    [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(val_Sg+1)), Xpre_train{i}, ...
            Xpre_test{i}, [], 0.03);
end
for i=ind_Sg(2,:)
    [Xpre_train{i}, Xpre_test{i}] = pretreat_scale(char(Scaling(1)), Xpre_train{i}, ...
            Xpre_test{i}, [], 0.03); %i.e. meancenter
end

if varsel==2 %remove variable from scaled data sets
    for i=1:16
        Xpre_train{i}(:, vars_removed{i}) = [];
        Xpre_test{i}(:, vars_removed{i}) = [];
    end
end

waitbar(0.9, h, 'Building PLS models...')
%Build PLS models
for i=1:16
    [B] = pls_Jan(Xpre_train{i}, Ytrain, complevec_exp(i));
    pred = Xpre_test{i} * B(2:end) + B(1);
    rmsepvec_exp(i) = rmse_Jan(Ytest, pred);
end


%Take log of rmsepvec_exp to make distribution more normal
rmsepvec_exp = log(rmsepvec_exp);


%Calculate effects
%Calculate 'real' effects 
eff(1) = mean(rmsepvec_exp(1:8)) - mean(rmsepvec_exp(9:16)); %baseline
eff(2) = mean(rmsepvec_exp([1:4 9:12])) - mean(rmsepvec_exp([5:8 13:16])); %scatter
eff(3) = mean(rmsepvec_exp([1 2 5 6 9 10 13 14])) - ...
    mean(rmsepvec_exp([3 4 7 8 11 12 15 16])); %smoothing
eff(4) = mean(rmsepvec_exp(1:2:16)) - mean(rmsepvec_exp(2:2:16)); %scaling
%2nd order interaction effect
eff(5) = mean(rmsepvec_exp([1:4 13:16])) - ...
    mean(rmsepvec_exp([5:12])); %baseline scatter
eff(6) = mean(rmsepvec_exp([1 2 5 6 11 12 15 16])) - ...
    mean(rmsepvec_exp([3 4 7 8 9 10 13 14]));%baseline smoothing
eff(7) = mean(rmsepvec_exp([1 3 5 7 10 12 14 16])) - ...
    mean(rmsepvec_exp([2 4 6 8 9 11 13 15]));%baseline scaling
eff(8) = mean(rmsepvec_exp([1 2 7:10 15 16])) - ...
    mean(rmsepvec_exp([3:6 11:14])); %scatter smoothing
eff(9) = mean(rmsepvec_exp([1 3 6 8 9 11 14 16])) - ...
    mean(rmsepvec_exp([2 4 5 7 10 12 13 15])); %scatter scaling
eff(10) = mean(rmsepvec_exp([1 4 5 8 9 12 13 16])) - ...
    mean(rmsepvec_exp([2 3 6 7 10 11 14 15])); %smoothing scaling

close(h)

%Make effects plot
set(handles.DoE_figure,'CurrentAxes',handles.effects_plot);
b = bar(1:10, diag(eff(1:10)), 'stacked');
Labels{1} = {'B','St','Sm','Sg',...
    'BxSt','BxSm','BxSg','StxSm','StxSg','SmxSg'};
set(gca,'XTickLabel',Labels{1})
set(gca, 'XTickLabelRotation', -30);
ylabel('log Effect')
for i=1:4
    b(i).FaceColor = 'red';
    b(i).EdgeColor = 'black';
end
for i=5:10
    b(i).FaceColor = 'blue';
    b(i).EdgeColor = 'black';
end
set(handles.text8, 'String', 'Effects based on RMSEP')
drawnow();

%Save variables required for Step 2 (is anything required for step 2?)
setappdata(handles.DoE_figure,'eff',eff);

%Provide recommendation in step 2, calculate total effects
efftotal(1) = eff(1); %baseline
efftotal(2) = eff(2); %scatter
efftotal(3) = eff(3); %smoothing
efftotal(4) = eff(4); %scaling
efftotal(5) = eff(1) + eff(2) + eff(5); %baseline+scatter
efftotal(6) = eff(1) + eff(3) + eff(6); %baseline+smoothing
efftotal(7) = eff(1) + eff(4) + eff(7); %baseline+scaling
efftotal(8) = eff(2) + eff(3) + eff(8); %scatter+smoothing
efftotal(9) = eff(2) + eff(4) + eff(9); %scatter+scaling
efftotal(10) = eff(3) + eff(4) + eff(10); %smoothing+scaling
efftotal(11) = eff(1) + eff(2) + eff(3) + eff(5) + eff(6) + eff(8); %baseline+scatter+smoothing
efftotal(12) = eff(1) + eff(2) + eff(4) + eff(5) + eff(7) + eff(9); %baseline+scatter+scaling
efftotal(13) = eff(1) + eff(3) + eff(4) + eff(6) + eff(7) + eff(10); %baseline+smoothing+scaling
efftotal(14) = eff(2) + eff(3) + eff(4) + eff(8) + eff(9) + eff(10); %scatter+smoothing+scaling
efftotal(15) = sum(eff); %baseline+scatter+smoothing+scaling

effmatrix = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1; 1 1 0 0; 1 0 1 0; 1 0 0 1;
    0 1 1 0; 0 1 0 1; 0 0 1 1; 1 1 1 0; 1 1 0 1; 1 0 1 1; 0 1 1 1; 1 1 1 1];

[~, relevant_steps] = min(efftotal);

set(handles.checkbox_B, 'Value', effmatrix(relevant_steps, 1))
set(handles.checkbox_St, 'Value', effmatrix(relevant_steps, 2))
set(handles.checkbox_Sm, 'Value', effmatrix(relevant_steps, 3))
set(handles.checkbox_Sg, 'Value', effmatrix(relevant_steps, 4))

%Display final message
set(handles.text_status, 'String', 'Finished! Proceed with step 2. Recommended preprocessing steps are indicated.')
drawnow();

%Activate next button:
set(handles.pushbutton_Optimize, 'Enable', 'On');


% --- Executes on selection change in popupmenu_B.
function checkbox_B_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_B contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_B

% --- Executes on selection change in popupmenu_B.
function checkbox_St_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_B contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_B

% --- Executes on selection change in popupmenu_B.
function checkbox_Sm_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_B contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_B

% --- Executes on selection change in popupmenu_B.
function checkbox_Sg_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_B contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_B


% --- Executes on selection change in popupmenu_ppB.
function popupmenu_ppB_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_ppB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_ppB contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_ppB


% --- Executes during object creation, after setting all properties.
function popupmenu_ppB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_ppB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_ppSt.
function popupmenu_ppSt_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_ppSt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_ppSt contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_ppSt


% --- Executes during object creation, after setting all properties.
function popupmenu_ppSt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_ppSt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_ppSm.
function popupmenu_ppSm_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_ppSm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_ppSm contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_ppSm


% --- Executes during object creation, after setting all properties.
function popupmenu_ppSm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_ppSm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_ppSg.
function popupmenu_ppSg_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_ppSg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_ppSg contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_ppSg


% --- Executes during object creation, after setting all properties.
function popupmenu_ppSg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_ppSg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_preprocess.
function pushbutton_preprocess_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_preprocess (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Load data
Xtrain = getappdata(handles.DoE_figure, 'Xtrain');
Xtest = getappdata(handles.DoE_figure, 'Xtest');
Ytrain = getappdata(handles.DoE_figure, 'Ytrain');
Ytest = getappdata(handles.DoE_figure, 'Ytest');
if isempty(Xtrain)
    message = 'You did not load Xtrain properly, try again!';
    uiwait(warndlg(message));
    return
end

%Initialize default parameters
Scaling = {'None', 'MC', 'AS', 'Range', 'Pareto', 'Poisson', 'Level', 'Log'};

%Read parameters
AsLS_param = getappdata(handles.DoE_figure, 'AsLS_param');
Savgol_B = getappdata(handles.DoE_figure, 'Savgol_B');
win = getappdata(handles.DoE_figure, 'Savgol_Sm_window');
ord = getappdata(handles.DoE_figure, 'Savgol_Sm_order');

Smooth = [win(1),win(2),win(3),win(1),win(2),win(3),win(1),win(2),win(3); ...
    ord(1),ord(1),ord(1),ord(2),ord(2),ord(2),ord(3),ord(3),ord(3)];
% Set PR settings
par(1) = 15;
par(2) = 25;
par(3) = 35;
par(5) = 2;
par(6) = 3;
par(7) = 4;
par(8) = AsLS_param(1);
par(9) = AsLS_param(2);
par(10) = 0;
par(11) = 0;
par(12) = Savgol_B;

% Obtain settings for preprocessing
val_B = get(handles.popupmenu_ppB, 'Value')-1;
val_St = get(handles.popupmenu_ppSt, 'Value')-1;
val_Sm = get(handles.popupmenu_ppSm, 'Value')-1;
val_Sg = get(handles.popupmenu_ppSg, 'Value');

Xpre_train = Xtrain;
if isempty(Xtest)
else
    Xpre_test =  Xtest;
end


if val_B>0
    set(handles.text_status, 'String', 'Performing baseline correction...')
    drawnow();
    Xpre_train = preprocess_B(Xpre_train, val_B, par);
    if isempty(Xtest)
    else
        Xpre_test =  preprocess_B(Xpre_test, val_B, par);
    end
end


if val_St>0
    set(handles.text_status, 'String', 'Performing scatter correction...')
    drawnow();
    Xpre_train = preprocess_St(Xpre_train, val_St, par);
    if isempty(Xtest)
    else
        Xpre_test =  preprocess_St(Xpre_test, val_St, par);
    end
end


if val_Sm>0
    set(handles.text_status, 'String', 'Performing smoothing...')
    drawnow();
    Xpre_train = pretreat('savgol', Xpre_train, Smooth(1,val_Sm), ...
        Smooth(2,val_Sm), 0);
    if isempty(Xtest)
    else
        Xpre_test = pretreat('savgol', Xpre_test, Smooth(1,val_Sm), ...
            Smooth(2,val_Sm), 0);
    end
end


set(handles.text_status, 'String', 'Performing scaling...')
drawnow();

if isempty(Xtest)
    [Xpre_train, ~] = pretreat_scale(char(Scaling(val_Sg)), Xpre_train, ...
            Xpre_train, [], 0.03);
else
    [Xpre_train, Xpre_test] = pretreat_scale(char(Scaling(val_Sg)), Xpre_train, ...
            Xpre_test, [], 0.03); %i.e. meancenter
end


assignin('base','Xtrain_pp', Xpre_train);
if isempty(Xtest)
else
    assignin('base','Xtest_pp', Xpre_test);
end

%Save data:
setappdata(handles.DoE_figure,'Xtrain_pp',Xpre_train);
setappdata(handles.DoE_figure,'Xtest_pp',Xpre_test);

%Update plot:
set(handles.DoE_figure,'CurrentAxes',handles.effects_plot);
p1 = plot(Xpre_train', 'k')
hold on
p2 = plot(Xpre_test', 'r')
hold off
legend([p1(1) p2(1)], {'Xtrain',  'Xtest'}, 'Location', 'Best');
set(handles.text8,'String', 'Preprocessed X data')


%Update interface:
set(handles.resultB, 'String', '');
set(handles.resultSt, 'String', '');
set(handles.resultSm, 'String', '');
set(handles.resultSg, 'String', '');
set(handles.text_RMSEP, 'String', '');
set(handles.text_no_of_LVs, 'String', '')
set(handles.pushbutton_save_data_matlab, 'Enable', 'On');
set(handles.pushbutton_save_data_excel, 'Enable', 'On');
set(handles.pushbutton_save_data_csv, 'Enable', 'On');

set(handles.text_status, 'String', 'Finished!')
drawnow();



% --- Executes on button press in pushbutton_clear_seqopt.
function pushbutton_clear_seqopt_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_clear_seqopt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Reset data:
Xtrain = [];
Xtest = [];
Ytrain = [];
Ytest = [];

%Reset plot:
legend('off')
set(handles.DoE_figure,'CurrentAxes',handles.effects_plot);
cla
set(handles.text8,'String', '')

%Reset GUI:
set(handles.text_status, 'String', '')
set(handles.checkbox_B, 'Value', 0);
set(handles.checkbox_St, 'Value', 0);
set(handles.checkbox_Sm, 'Value', 0);
set(handles.checkbox_Sg, 'Value', 0);
set(handles.resultB, 'String', '');
set(handles.resultSt, 'String', '');
set(handles.resultSm, 'String', '');
set(handles.resultSg, 'String', '');
set(handles.text_RMSEP, 'String', '');
set(handles.text_no_of_LVs, 'String', '')
set(handles.popupmenu_ppB, 'Value', 1);
set(handles.popupmenu_ppSt, 'Value', 1);
set(handles.popupmenu_ppSm, 'Value', 1);
set(handles.popupmenu_ppSg, 'Value', 1);
set(handles.pushbutton_save_data_matlab, 'Enable', 'Off');
set(handles.pushbutton_save_data_excel, 'Enable', 'Off');
set(handles.pushbutton_save_data_csv, 'Enable', 'Off');
set(handles.pushbutton_CalculateDoE, 'Enable', 'Off');
set(handles.pushbutton_Optimize, 'Enable', 'Off');
set(handles.pushbutton_save_data_matlab, 'Enable', 'Off');
set(handles.pushbutton_save_data_excel, 'Enable', 'Off');
set(handles.pushbutton_save_data_csv, 'Enable', 'Off');
set(handles.pushbutton_preprocess, 'Enable', 'Off');
drawnow()


% --- Executes on button press in pushbutton_dialog_pp_parameters.
function pushbutton_dialog_pp_parameters_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_dialog_pp_parameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

prompt = {'Maximum no. of LVs during cross-validation:', 'Fraction of samples to leave out during cross-validation:', 'Baseline: AsLS parameters (lamda p):', ...
    'Baseline: window width Savitzky-Golay differentiation:', 'Smoothing: window widths Savitzky-Golay (specify 3):', ...
    'Smoothing: polynomial orders Savitzky-Golay (specify 3):'};
dlg_title = 'Set parameters';
num_lines = 1;
def = {num2str(getappdata(handles.DoE_figure, 'MaxLV')), ...
    num2str(getappdata(handles.DoE_figure, 'CV_perc')), ...
    num2str(getappdata(handles.DoE_figure, 'AsLS_param')), ...
    num2str(getappdata(handles.DoE_figure, 'Savgol_B')), ...
    num2str(getappdata(handles.DoE_figure, 'Savgol_Sm_window')), ...
    num2str(getappdata(handles.DoE_figure, 'Savgol_Sm_order'))};
options.Resize='off';
options.WindowStyle = 'normal';
options.Interpreter = 'none';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);

if size(answer,1)==0
    set(handles.text_status, 'String', 'You pressed cancel: preprocessing parameters have not been updated.')
else
    setappdata(handles.DoE_figure, 'MaxLV', str2num(answer{1}))
    setappdata(handles.DoE_figure, 'CV_perc', str2num(answer{2}))
    setappdata(handles.DoE_figure, 'AsLS_param', str2num(answer{3}))
    setappdata(handles.DoE_figure, 'Savgol_B', str2num(answer{4}))
    setappdata(handles.DoE_figure, 'Savgol_Sm_window', str2num(answer{5}))
    setappdata(handles.DoE_figure, 'Savgol_Sm_order', str2num(answer{6}))
    set(handles.text_status, 'String', 'Successfully updated preprocessing parameters.')
    
    %Update strings in selection boxes
    Sm = get(handles.popupmenu_Sm, 'String');
    ppSm = get(handles.popupmenu_ppSm, 'String');
    winmat = [1 2 3 1 2 3 1 2 3];
    ordmat = [1 1 1 2 2 2 3 3 3];
    winval = str2num(answer{5});
    ordval = str2num(answer{6});
    for i=1:9
        Sm{i} = ['Window ', num2str(winval(winmat(i))), ', order ', num2str(ordval(ordmat(i)))];
        ppSm{i+1} = ['Window ', num2str(winval(winmat(i))), ', order ', num2str(ordval(ordmat(i)))];
    end
    set(handles.popupmenu_Sm, 'String', Sm);
    set(handles.popupmenu_ppSm, 'String', ppSm);    
end


% --- Executes on selection change in popupmenu_varselect.
function popupmenu_varselect_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_varselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_varselect contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_varselect


% --- Executes during object creation, after setting all properties.
function popupmenu_varselect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_varselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_save_data_excel.
function pushbutton_save_data_excel_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save_data_excel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    [fileName,pathName] = uiputfile('*.xlsx','Save preprocessed data (Excel)');
    excelFile=fullfile(pathName,fileName);
    Xtrain_pp = getappdata(handles.DoE_figure, 'Xtrain_pp');
    Xtest_pp = getappdata(handles.DoE_figure, 'Xtest_pp');
    Ytrain = getappdata(handles.DoE_figure, 'Ytrain');
    Ytest = getappdata(handles.DoE_figure, 'Ytest');
    xlswrite(excelFile, Xtrain_pp, 'Xtrain')
    xlswrite(excelFile, Xtest_pp, 'Xtest')
    xlswrite(excelFile, Ytrain, 'Ytrain')
    xlswrite(excelFile, Ytest, 'Ytest')
    set(handles.text_status, 'String', 'File succesfully saved.')
catch
    set(handles.text_status, 'String', 'File saving cancelled or failed.')
end


% --- Executes during object creation, after setting all properties.
function pushbutton_clear_seqopt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton_clear_seqopt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



% --- Executes during object creation, after setting all properties.
function pushbutton_LoadExcel_CreateFcn(hObject, eventdata, handles)
% hObject    pushbutton_LoadExcel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on mouse press over axes background.
function coast_logo_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to coast_logo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dos('start https://www.ti-coast.com/');
display('Coast')


% --- Executes on mouse press over axes background.
function ru_logo_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to ru_logo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dos('start https://www.ru.nl/science/analyticalchemistry/');
display('RU')


% --- Executes on button press in pushbutton_save_data_csv.
function pushbutton_save_data_csv_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save_data_csv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    [fileName,pathName] = uiputfile('*.csv','Save preprocessed data (CSV)');
    Xtrain_pp = getappdata(handles.DoE_figure, 'Xtrain_pp');
    Xtest_pp = getappdata(handles.DoE_figure, 'Xtest_pp');
    Ytrain = getappdata(handles.DoE_figure, 'Ytrain');
    Ytest = getappdata(handles.DoE_figure, 'Ytest');
    Subset = string([repmat({'Train'}, size(Xtrain_pp, 1), 1); repmat({'Test'}, size(Xtest_pp, 1), 1)]);
    Y = [Ytrain; Ytest];
    X = [Xtrain_pp; Xtest_pp];
    datatable = table(Subset, Y, X);
    writetable(datatable, fullfile(pathName,fileName));
    set(handles.text_status, 'String', 'File succesfully saved.')
catch
    set(handles.text_status, 'String', 'File saving cancelled or failed.')
end


% --- Executes on button press in pushbutton_LoadMatlab.
function pushbutton_LoadMatlab_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_LoadMatlab (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileName,pathName] = uigetfile('*.mat','Load raw data (Matlab)');
% Check if cancel pressed
if isequal(fileName,0)
    set(handles.text_status, 'String', 'No data file was selected')
    return
end

set(handles.text_status, 'String', 'Loading data...')
drawnow();

% Read data in Matlab file
matlabFile=fullfile(pathName,fileName);
try 
    load(matlabFile, 'Xtrain');
catch
    message = 'Cannot load Xtrain. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

try 
    load(matlabFile, 'Xtest');
catch
    message = 'Cannot load Xtest. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

try 
    load(matlabFile, 'Ytrain');
catch
    message = 'Cannot load Ytrain. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

try 
    load(matlabFile, 'Ytest');
catch
    message = 'Cannot load Ytest. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

%Perform some checks of the data
if size(Xtrain,1) ~= size(Ytrain, 1)
    message =  'The number of samples in Xtrain and Ytrain differ! Please check. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

if size(Xtest,1) ~= size(Ytest, 1)
    message =  'The number of samples in Xtest and Ytest differ! Please check. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end

if size(Xtest,2) ~= size(Xtrain, 2)
    message =  'The number of variables in Xtrain and Xtest is unequal! Please check. Data has not been loaded.';
    uiwait(warndlg(message));
    set(handles.text_status, 'String', 'Error loading data!')
    drawnow();
    return
end
    
setappdata(handles.DoE_figure,'Xtrain',Xtrain);
setappdata(handles.DoE_figure,'Xtest',Xtest);
setappdata(handles.DoE_figure,'Ytrain',Ytrain);
setappdata(handles.DoE_figure,'Ytest',Ytest);
%Reserve memory for preprocessed data:
setappdata(handles.DoE_figure,'Xtrain_pp',Xtrain);
setappdata(handles.DoE_figure,'Xtest_pp',Xtest);

set(handles.DoE_figure,'CurrentAxes',handles.effects_plot);
%make plot of Xtrain and Xtest
p1 = plot(Xtrain', 'k', 'DisplayName', 'Xtrain');
hold on
p2 = plot(Xtest', 'r', 'DisplayName', 'Xtest');
hold off
legend([p1(1) p2(1)], {'Xtrain',  'Xtest'}, 'Location', 'Best');
set(handles.text8,'String', 'Raw X data')
set(handles.text_status, 'String', 'Successfully loaded the data')
set(handles.DoE_figure, ...
    'Name',sprintf('Preprocessing Toolbox - ? Jan Gerretzen, 2016, Radboud University. Current data file: %s', fileName), ...
    'NumberTitle','off');

%Activate the next button:
set(handles.pushbutton_CalculateDoE, 'Enable', 'On');
set(handles.pushbutton_preprocess, 'Enable', 'On');


% --- Executes on button press in pushbutton_save_data_matlab.
function pushbutton_save_data_matlab_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save_data_matlab (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    [fileName,pathName] = uiputfile('*.mat','Save preprocessed data (Matlab)');
    matlabFile=fullfile(pathName,fileName);
    Xtrain = getappdata(handles.DoE_figure, 'Xtrain_pp');
    Xtest = getappdata(handles.DoE_figure, 'Xtest_pp');
    Ytrain = getappdata(handles.DoE_figure, 'Ytrain');
    Ytest = getappdata(handles.DoE_figure, 'Ytest');
    save(matlabFile, 'Xtrain', 'Xtest', 'Ytrain', 'Ytest');
    set(handles.text_status, 'String', 'File succesfully saved.')
catch
    set(handles.text_status, 'String', 'File saving cancelled or failed.')
end


% --- Executes on button press in pushbutton_MakeReport.
function pushbutton_MakeReport_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_MakeReport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%try

save('reportdata.mat', 'handles', '-v7.3');
makereport;
delete('reportdata.mat');

publish('makereport', 'format', 'pdf', 'showCode', false);

%catch
%    set(handles.text_status, 'String', 'Report could not be made.')
%end