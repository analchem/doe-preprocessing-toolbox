function makereport()

display('Generating PDF...');

%Load report data:
load('reportdata.mat');

%Plot raw data:
figure;
hold on;
p1 = plot(getappdata(handles.DoE_figure, 'Xtrain')', 'k');
p2 = plot(getappdata(handles.DoE_figure, 'Xtest')', 'r');
xlabel('Spectral variable');
ylabel('Signal');
legend([p1(1); p2(1)], 'Training data', 'Testing data', 'Location', 'Best', 'AutoUpdate', 'off');



end